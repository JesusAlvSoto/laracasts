@extends('app')

@section('content')
	<h1>About</h1>
	<h3>People I like <span class="badge">{{ count($friendsArray) }}</span></h3>
	@if(count($friendsArray) > 0)
	    <ul class="list-group">
        @foreach($friendsArray as $friend)
            <li class="list-group-item">{{ $friend }}</li>
        @endforeach
        </ul>
    @endif
@stop