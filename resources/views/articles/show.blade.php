@extends('app')

@section('content')
    <h1>{{ $article->title }}</h1>
    <cite>{{ $article->published_at->diffForHumans() }}</cite>
    <article>
        <div class="body">{{ $article->body }}</div>
    </article>
    @unless($article->tags->isEmpty())
        <h5>Tags:</h5>
        <ul>
        @foreach($article->tags as $tag)
            <span class="badge">{{ $tag->name }}</span>
        @endforeach
        </ul>
    @endunless
    <a href="{{ url('/articles', [$article->id, 'edit']) }}">Edit article</a>
@stop