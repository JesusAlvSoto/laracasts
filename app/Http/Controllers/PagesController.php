<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

    public function about()
    {
        $friendsArray = ['Jesus', 'Alicia', 'Amit', 'Rafael', 'Sergio'];
        return view('pages.about', compact('friendsArray'));
    }

    public function contact()
    {
        return view('pages.contact');
    }
}
