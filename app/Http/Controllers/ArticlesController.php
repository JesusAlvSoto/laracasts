<?php namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use App\Http\Requests\ArticleRequest;
use App\Tag;

class ArticlesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth', ['only' => 'create']);
    }

    /*
     * Show all articles
     *
     * @returns Response
     */
    public function index()
    {
        $articles = Article::latest('published_at')->published()->get();

        return view('articles.index', compact('articles'));
    }

    /*
     * Shows details of an article
     *
     * @returns Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.show', compact('article'));
    }

    public function create()
    {
        $tags = Tag::lists('name', 'id');

        return view('articles.create', compact('tags'));
    }

    /*
     * Save a new article
     *
     * @params CreateArticleRequest $request
     * @return Response
     */
    public function store(ArticleRequest $request)
    {
        //now get the authenticated user's articles and create a new article with the data passed in the request.
        //in the background Laravel will assign the user_id to that article.
        $article = \Auth::user()->articles()->create($request->all());

        $tagIds = $request->input('tag_list');

        $article->tags()->attach($tagIds);

        session()->flash('flash_message', 'Your article has been created!');
        session()->flash('flash_message_important', 'very important stuff going on here');

        return redirect('articles');
    }

    public function edit($article_id)
    {
        $article = Article::findOrFail($article_id);

        $tags = Tag::lists('name', 'id');

        return view('articles.edit', compact('article', 'tags'));
    }

    public function update($id, ArticleRequest $request)
    {
        $article = Article::findOrFail($id);

        $article->update($request->all());

        return redirect('articles');
    }
}
